import React from "react";
import { toast } from 'react-toastify';

class AddToDo extends React.Component {
    state = {
        title: ''
    }

    handleOnChangeTitle = (event) => {
        this.setState({
            title: event.target.value
        })
    }

    handleAddToDo = () => {
        // alert("click me");


        //validate data
        if (!this.state.title) {
            toast.error("Title rong");

            return;
        }
        let todo = {
            id: Math.floor(Math.random() * 10000),
            title: this.state.title
        }
        this.props.addNewToDo(todo);
        this.setState({
            title: ''
        })
    }
    render() {
        let { title } = this.state;
        return (
            <div className="add-todo" >
                <input type={"text"} value={title} onChange={(event) => {
                    this.handleOnChangeTitle(event)
                }} />
                <button
                    onClick={() => {
                        this.handleAddToDo()
                    }}
                    type="button" className="add">Add</button>
            </div>
        )
    }



}


export default AddToDo