import React from "react";
import AddToDo from "./AddToDo";
import './ListToDo.scss'
import './AddToDo'
import { toast } from 'react-toastify';

class ListToDo extends React.Component {
    state = {
        ListToDos: [
            { id: 'todo1', title: 'Doing homework' },
            { id: 'todo2', title: 'Making videos' },
            { id: 'todo3', title: 'Fixing bugs' },
        ],
        editToDo: {

        }

    }

    addNewToDo = (todo) => {
        this.setState({
            ListToDos: [...this.state.ListToDos, todo]

        })
        toast.success("Wow so easy!");

    }
    handleDeleteToDo = (todo) => {
        // alert("delete")
        // console.log("check todo", todo);
        let currentToDos = this.state.ListToDos;
        currentToDos = currentToDos.filter(item => item.id !== todo.id)
        this.setState({
            ListToDos: currentToDos
        })
        toast.success("Delete succeed!");

    }
    handleEditToDo = (todo) => {
        let { editToDo, ListToDos } = this.state;



        let isEmptyObj = Object.keys(editToDo).length === 0
        if (!isEmptyObj && editToDo.id === todo.id) {

            let ListToDosCopy = [...ListToDos]

            //Find index of specific object using findIndex method.    
            let objIndex = ListToDosCopy.findIndex((item => item.id === todo.id));



            //Update object's name property.
            ListToDosCopy[objIndex].title = editToDo.title;
            this.setState({
                ListToDos: ListToDosCopy,
                editToDo: {}
            })
            toast.success("Update Todo succeed!");

            return;
        }

        this.setState({
            editToDo: todo
        })



    }
    handleOnChangeEditToDo = (event) => {
        let editToDoCopy = { ...this.state.editToDo };
        editToDoCopy.title = event.target.value;
        this.setState({
            editToDo: editToDoCopy
        })
    }
    render() {
        // tạo biến gọi đến state
        let { ListToDos, editToDo } = this.state
        let isEmptyObj = Object.keys(editToDo).length === 0
        console.log("check empty object", isEmptyObj);
        return (
            <>
                <p>
                    Simple TODO Apps with React.js ( VietTri 	&cent; Viet Tri )
                </p>
                <div className="list-todo-container" >
                    <AddToDo
                        addNewToDo={this.addNewToDo}
                    />
                    <div className="list-todo-content" >

                        {ListToDos && ListToDos.length > 0 &&
                            ListToDos.map((item, index) => {
                                return (
                                    <div className="todo-child" key={item.id}>
                                        {isEmptyObj === true ?
                                            <span>{index + 1}- {item.title}</span>
                                            :
                                            <>
                                                {
                                                    editToDo.id === item.id ?
                                                        <span>
                                                            {index + 1} - <input value={editToDo.title}
                                                                onChange={(event) => {
                                                                    this.handleOnChangeEditToDo(event)
                                                                }}

                                                            />
                                                        </span>
                                                        :
                                                        <span>{index + 1}- {item.title}</span>

                                                }
                                            </>

                                        }

                                        <button onClick={() => {
                                            this.handleEditToDo(item)
                                        }} className="edit">

                                            {!isEmptyObj && editToDo.id === item.id ? "Save" : "Edit"}
                                            {/* Edit */}



                                        </button>
                                        <button onClick={() => {
                                            this.handleDeleteToDo(item)
                                        }} className="delete" >Delete</button>

                                    </div>
                                )

                            })

                        }




                    </div>


                </div>
            </>
        )
    }
}



export default ListToDo;