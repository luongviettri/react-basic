import React from "react";
class AddComponent extends React.Component {
    state = {
        title: '',
        salary: ''
    }
    handleChangeTitle(event) {
        this.setState({
            title: event.target.value
        })
    }
    handleChangeSalary(event) {
        this.setState({
            salary: event.target.value
        })
    }
    handleSubmit = (event) => {
        event.preventDefault();
        if (!this.state.title || !this.state.salary) {
            alert("vui long nhap du thong tin");
            return;
        }
        console.log("da nhan duoc state", this.state);
        this.props.addNewJob({
            id: Math.floor(Math.random() * 1001),
            title: this.state.title,
            salary: this.state.salary
        })
        this.setState({
            title: '',
            salary: ''
        })


    }
    render() {
        return (
            <form action="/action_page.php">
                <label htmlFor="fname">Job's title:</label><br />
                <input type="text" onChange={(event) => {
                    this.handleChangeTitle(event)
                }}
                    value={this.state.title}

                /><br />
                <label htmlFor="lname">Salary:</label><br />
                <input type="text" id="lname" name="lname"
                    value={this.state.salary}
                    onChange={(event) => {
                        this.handleChangeSalary(event)
                    }}

                />



                <br />

                <input onClick={(event) => {
                    this.handleSubmit(event)
                }} type="submit" value="Submit" />
            </form>
        )
    }
}


export default AddComponent;