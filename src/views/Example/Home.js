import React from "react";
import { withRouter } from "react-router";
import color from "../HOC/color";
import logo from "../../assets/images/avatar.jpg"
import { connect } from 'react-redux';
class Home extends React.Component {

    componentDidMount() {
        // console.log(">>>>Check timeout");
        setTimeout(() => {
            this.props.history.push("/todo");
        }, 3000);
    }

    handleDeleteUser(user) {
        // console.log("chay duco ham handleDeleteUser", user);
        this.props.deleteUserRedux(user);
    }
    handleCreateUser() {
        // console.log("add");
        this.props.addUserRedux();
    }
    render() {
        let listUsers = this.props.dataRedux;

        return (
            <>
                <div>Hello world from home page</div>
                <div>
                    <img src={logo} style={{
                        width: "200px",

                        marginTop: "20px"
                    }} />
                </div>

                <div>
                    {listUsers && listUsers.length > 0 &&
                        listUsers.map((item, index) => {
                            return (
                                <div key={item.id} >
                                    {index + 1}- {item.name}
                                    &nbsp;   <span onClick={() => {
                                        this.handleDeleteUser(item)
                                    }} >x</span>

                                </div>
                            )
                        })
                    }
                    <button onClick={() => {
                        this.handleCreateUser()
                    }}>Add new</button>
                </div>


            </>
        )
    }
}
// export default withRouter(Home)

/**
 * hàm mapStateToProps dùng để nhận dữ liệu từ state của redux về props
 * @param {*} state : state của redux
 * @returns : object
 */
const mapStateToProps = (state) => {

    // tao 1 bien ten la dataRedux
    return {
        dataRedux: state.users
    }
}
/**
 *  truyền action dạng tên action, dữ liệu cần truyền (có hoặc ko )
 * @param {*} dispatch : action cần truyền
 */
const mapDispatchToProps = (dispatch) => {
    return {
        deleteUserRedux: (userDelete) => {
            dispatch({ type: "DELETE_USER", payload: userDelete })
        },
        addUserRedux: () => {
            dispatch({ type: "CREATE_USER" })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(color(Home));
