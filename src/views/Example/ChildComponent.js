import React from 'react';
import './Demo.scss'
class ChildComponent extends React.Component {

    state = {
        showJobs: false
    }

    handleShowHide = () => {
        this.setState({
            showJobs: !this.state.showJobs
        })
    }

    handleOnClickDelete = (job) => {
        this.props.deleteAJob(job)
    }



    render() {

        let { arrProps } = this.props;
        let { showJobs } = this.state
        return (


            <>
                {showJobs === false ?
                    <div>
                        <button style={{ color: "red" }} onClick={() => { this.handleShowHide() }}>
                            Show</button>
                    </div>
                    :
                    <>
                        <div className='jobsList'>
                            {arrProps.map((item, index) => {
                                return (
                                    <>

                                        <div key={item.id}>

                                            {item.title} - {item.salary} <></>
                                            <span onClick={() => {
                                                this.handleOnClickDelete(item)
                                            }}>x</span>

                                        </div>


                                    </>
                                )
                            })}
                        </div>


                        <div> <button
                            onClick={() => {
                                this.handleShowHide()
                            }}
                        >Hide</button> </div>
                    </>
                }
            </>


        )
    }
}

export default ChildComponent;