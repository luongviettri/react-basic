import React from 'react';
import ChildComponent from './ChildComponent';

import AddComponent from './AddComponent';
class MyComponent extends React.Component {

    //dinh nghia state
    state = {

        arrJobs: [
            { id: "a1", title: "dev", salary: "500u" },
            { id: "a2", title: "designer", salary: "400u" },
            { id: "a3", title: "pm", salary: "1000u" }
        ]
    }

    addNewJob = (job) => {

        this.setState({
            arrJobs: [...this.state.arrJobs, job]
        })
        console.log("check job from parent", job);
    }
    deleteAJob = (job) => {
        let currentJobs = this.state.arrJobs;

        currentJobs = currentJobs.filter(item => item.id !== job.id);
        this.setState({
            arrJobs: currentJobs
        })
    }

    render() {
        console.log("render", this.state);

        return (
            <>
                <AddComponent

                    addNewJob={this.addNewJob}
                />

                <ChildComponent

                    arrProps={this.state.arrJobs}
                    deleteAJob={this.deleteAJob}
                />


            </>

        )
    }
}
export default MyComponent;